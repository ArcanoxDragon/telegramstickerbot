import { EntityRepository, Repository } from "typeorm";
import { ChatUserDto } from "@/models/data/ChatUserDto";

@EntityRepository( ChatUserDto )
export class UserRepository extends Repository<ChatUserDto> {
    public async findOrCreate( chatId: number, userId: number ) {
        let userDto = await this.findOne( { chatId, userId } );

        if ( !userDto ) {
            userDto = this.create( { chatId, userId } );

            await this.save( userDto );
        }

        return userDto;
    }
}