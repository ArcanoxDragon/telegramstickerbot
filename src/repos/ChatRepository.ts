import { Repository, EntityRepository } from "typeorm";
import { ChatDto } from "@/models/data/ChatDto";
import { ModerationConfig } from "@/models/config";
import { container } from "tsyringe";
import { ConfigManager } from "@/managers/ConfigManager";
import { MessageDto } from "@/models/data/MessageDto";

import moment from "moment";

@EntityRepository( ChatDto )
export class ChatRepository extends Repository<ChatDto> {
    private config: ModerationConfig;

    constructor() {
        super();

        let configManager = container.resolve( ConfigManager );

        this.config = configManager.config.moderation;
    }

    public async findOrCreate( chatId: number ) {
        let chatDto = await this.findOne( { id: chatId } );

        if ( !chatDto ) {
            chatDto = this.create( { id: chatId } );

            await this.save( chatDto );
        }

        return chatDto;
    }

    public async isOverThresholdAsync( chatId: number ) {
        let chatDto = await this.findOrCreate( chatId );
        let moderationThreshold = chatDto.moderationThreshold ?? this.config.moderationThreshold;
        let noiseValue = await this.getChatNoiseValueAsync( chatId );

        return noiseValue >= moderationThreshold;
    }

    public async getChatNoiseValueAsync( chatId: number ) {
        // Retrieve metadata about chat
        let chatDto = await this.findOrCreate( chatId );
        let bucketSize = chatDto.bucketSize ?? this.config.bucketSize;
        let stickerNoiseScore = chatDto.stickerNoiseScore ?? this.config.stickerNoiseScore;
        let normalNoiseScore = chatDto.normalNoiseScore ?? this.config.normalNoiseScore;
        let messageDropoffTime = moment.utc().subtract( chatDto.messageDropoffTime ?? this.config.messageDropOffTime, "seconds" );

        // Retrieve and aggregate messages
        let messageRepo = this.manager.getRepository( MessageDto );
        let messages = await messageRepo.find( {
            where: { chatId },
            order: { id: "DESC" },
            take: bucketSize,
        } );
        let sum = messages.reduce( ( sum, message ) => {
            let messageSentTime = moment.utc( message.date );

            if ( messageSentTime.isBefore( messageDropoffTime ) ) {
                return 0;
            }

            let noiseScore = message.type === "sticker" ? stickerNoiseScore : normalNoiseScore;

            return sum + noiseScore;
        }, 0 );

        return sum / ( chatDto.bucketSize ?? this.config.bucketSize );
    }
}