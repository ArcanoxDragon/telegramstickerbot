const path = require( "path" );

process.env.TS_NODE_PROJECT = path.join( __dirname, "../tsconfig.json" );

require( "ts-node" ).register( { files: true } );
require( "tsconfig-paths" ).register();
require( "./app" ).main();