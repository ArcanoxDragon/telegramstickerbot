import "reflect-metadata";
import "moment-duration-format";

import { container } from "tsyringe";
import { ConfigManager } from "@/managers/ConfigManager";
import { BotManager } from "@/managers/BotManager";
import { createDataConnection } from "@/utility/data";
import { Connection } from "typeorm";
import { MessagePurgeManager } from "@/managers/MessagePurgeManager";

import path from "path";
import yargs from "yargs";

export async function main() {
    try {
        const { argv } = yargs.option( "config", {
            alias: "c",
            type: "string",
            description: "The path to the config file",
            demandOption: "Config path must be specified",
        } );
        const {
            config: configPath,
        } = argv;

        // Load/initialize the config
        let configManager = container.resolve( ConfigManager );
        let fullConfigPath = path.resolve( __dirname, "..", configPath )

        console.log( `Loading configuration file from "${fullConfigPath}"...` );
        await configManager.loadAsync( fullConfigPath );
        console.log( "Done loading config." );

        // Initialize data connection
        console.log( "Initializing database connection..." );
        let dataConnection = await createDataConnection( configManager.config.database );
        console.log( "Done initializing database connection." );

        container.register<Connection>( Connection, { useValue: dataConnection } );

        // Initialize the message purging subsystem
        let messagePurgeManager = container.resolve( MessagePurgeManager );

        console.log( "Initializing message purge subsystem..." );
        messagePurgeManager.start();
        console.log( "Done initializing message purge subsystem." );

        // Initialize the bot itself
        let bot = container.resolve( BotManager );

        console.log( "Initializing bot." );
        await bot.startAsync();
        console.log( "Bot is now running!" );

        async function onKill() {
            console.log( "Shutdown requested! Stopping message purge subsystem..." );
            messagePurgeManager.stop();

            console.log( "Stopping bot..." );
            await bot.stopAsync();

            console.log( "Closing database connection..." );
            await dataConnection.close();

            console.log( "Exiting!" );
            process.exit();
        }

        process.once( "SIGINT", onKill );
        process.once( "SIGTERM", onKill );
    } catch ( err ) {
        console.error( "=== Unhandled Exception ===" );
        console.error( err );
    }
}