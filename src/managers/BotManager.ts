import { singleton } from "tsyringe";
import { ConfigManager } from "@/managers/ConfigManager";
import { ConfigFile } from "@/models/config";
import { Message, Metadata, MessageType, BotCommand, CallbackQuery, InlineKeyboardButton } from "node-telegram-bot-api";
import { ModerationManager } from "@/managers/ModerationManager";
import { Connection } from "typeorm";
import { ChatRepository } from "@/repos/ChatRepository";

import TelegramBot from "node-telegram-bot-api";
import moment from "moment";
import { formatSeconds } from "@/utility/moment";

// #region Command Types

interface SentCommand {
    command: string;
    bot: string;
}

type InlineCommandType =
    | "configure";

interface BaseUserCommandState {
    command: InlineCommandType;
    userId: number;
    initiatingMessageId: number;
    promptMessageId?: number;
}

interface ConfigureUserCommandState extends BaseUserCommandState {
    command: "configure";
    action?: "history_size" | "history_expiration" | "moderation_threshold" | "warning_timeout" | "user_sticker_cooldown";
}

type UserCommandState =
    | ConfigureUserCommandState;

// #endregion Command Types

const DeleteMessagesDelay = 5000;
const AllCommands: BotCommand[] = [
    {
        command: "configure",
        description: "Configures settings for this bot in the current Group or Supergroup.",
    },
    {
        command: "cancel",
        description: "Cancels the current operation, if there is one.",
    },
    {
        command: "init",
        description: "Initializes the bot in this chat. Also happens when a setting is configured via the /configure command.",
    }
];

@singleton()
export class BotManager {
    private static readonly AcceptableMessageTypes: MessageType[] = [ "text", "audio", "document", "photo", "video", "voice", "sticker" ];

    private webhookUrl: string;
    private config: ConfigFile;
    private currentlyWarningUsers: Record<number, boolean>;
    private chatUserStates: Record<number, Record<number, UserCommandState>>;
    private chatRepo: ChatRepository;
    private bot: TelegramBot;

    constructor( configManager: ConfigManager, private moderationManager: ModerationManager, dataConnection: Connection ) {
        let { telegram, webhook } = configManager.config;

        this.webhookUrl = `${webhook.address}/${telegram.botToken}`;
        this.config = configManager.config;
        this.currentlyWarningUsers = {};
        this.chatUserStates = {};
        this.chatRepo = dataConnection.getCustomRepository( ChatRepository );
        this.bot = new TelegramBot( telegram.botToken, {
            webHook: {
                port: webhook.port,
                cert: webhook.sslCertPath,
                key: webhook.sslKeyPath,
            },
        } );

        // Initialize event listeners
        this.bot.on( "message", this.onMessage.bind( this ) );
        this.bot.on( "callback_query", this.onCallbackQuery.bind( this ) );
        this.bot.on( "webhook_error", this.onError.bind( this ) );
        this.bot.on( "error", this.onError.bind( this ) );
    }

    public async startAsync() {
        await this.bot.setMyCommands( AllCommands );
        await this.bot.setWebHook( this.webhookUrl, {} );
    }

    public async stopAsync() {
        await this.bot.setWebHook( "", {} ); // Remove the webhook
    }

    private async verifyCanDeleteAsync( message: Message ) {
        let me = await this.bot.getMe();
        let myMember = await this.bot.getChatMember( message.chat.id, me.id.toString() );

        if ( message.chat.type === "group" ) {
            return myMember.status === "creator" || myMember.status === "administrator";
        } else if ( message.chat.type === "supergroup" ) {
            return myMember.can_delete_messages === true;
        } else {
            return false;
        }
    }

    private async onMessage( message: Message, metadata: Metadata ) {
        try {
            if ( !metadata?.type || BotManager.AcceptableMessageTypes.indexOf( metadata.type as MessageType ) < 0 ) {
                // Ignore unwanted message types

                return;
            }

            if ( message.chat.type === "private" ) {
                await this.handlePrivateMessageAsync( metadata.type, message );
            } else if ( message.chat.type === "group" || message.chat.type === "supergroup" ) {
                await this.handleGroupMessageAsync( metadata.type, message );
            }
        } catch ( err ) {
            this.onError( err );
        }
    }

    private async handlePrivateMessageAsync( messageType: MessageType, message: Message ) {
        if ( messageType === "text" && message.from?.username === this.config.telegram.ownerUsername ) {
            // Send a ping back to the owner for confirmation that the bot is running

            await this.bot.sendMessage( message.chat.id, "Hi there!" );
        }
    }

    private async handleGroupMessageAsync( messageType: MessageType, message: Message ) {
        let me = await this.bot.getMe();
        let messageCommand = this.getCommand( message );

        if ( messageCommand && messageCommand.bot === me.username ) {
            await this.handleCommandAsync( messageCommand.command, message );
        } else {
            if ( message.reply_to_message && message.reply_to_message.from?.id === me.id ) {
                await this.handleReplyToMeAsync( message );
            } else {
                // Determine whether or not we should moderate the message *before* recording the entry
                let shouldModerate = await this.moderationManager.shouldModerateAsync( messageType, message );

                await this.moderationManager.recordEntryAsync( messageType, message );

                if ( shouldModerate ) {
                    // Remove the sticker

                    let noiseValue = await this.chatRepo.getChatNoiseValueAsync( message.chat.id );

                    console.log( `Removing message ${message.message_id} from ${message.from!.username}: noise value is ${noiseValue}` );

                    if ( await this.verifyCanDeleteAsync( message ) ) {
                        await this.bot.deleteMessage( message.chat.id, message.message_id.toString() );
                    } else {
                        console.error( `Error! I can't delete message ${message.chat.id}/${message.message_id} from user ${message.from?.username} because I don't have permission!` );
                    }

                    let fromUser = message.from!;

                    // Two stickers could arrive before we mark this user as "warned" in the DB. To avoid warning them twice in a row,
                    // we keep a transient flag in-memory saying that we're in the process of warning them.
                    if ( !this.currentlyWarningUsers[ fromUser.id ] && await this.moderationManager.shouldWarnSenderAsync( message ) ) {
                        this.currentlyWarningUsers[ fromUser.id ] = true;

                        let warnMessage =
                            `*Hey @${fromUser.username}*, please try to limit the amount of stickers being sent for a while\\!\n\n` +
                            `It seems there have been a lot of them lately, and it makes it difficult to have a conversation\\.`;

                        await this.moderationManager.notifyWarnedSenderAsync( message );
                        await this.bot.sendMessage( message.chat.id, warnMessage, { parse_mode: "MarkdownV2" } );

                        delete this.currentlyWarningUsers[ fromUser.id ];
                    }
                }
            }
        }
    }

    private getCommand( message: Message ): Nullable<SentCommand> {
        if ( !message.text || !message.entities || message.entities.length < 1 ) {
            return null;
        }

        let [ firstEntity ] = message.entities;

        if ( firstEntity.type != "bot_command" ) {
            return null;
        }

        let commandText = message.text.substr( firstEntity.offset, firstEntity.length );
        let commandMatch = commandText.match( /^\/([^@\s]+)(?:@([^\s]+))?/ );

        if ( !commandMatch ) {
            return null;
        }

        let [ , command, bot ] = commandMatch;

        return { command, bot };
    }

    private async handleCommandAsync( command: string, message: Message ) {
        switch ( command ) {
            case "configure":
                if ( await this.verifyCommandPermissionsAsync( message ) ) {
                    await this.handleConfigureCommandAsync( message );
                }

                break;
            case "cancel":
                if ( message.from ) {
                    let currentChatUserStates = this.chatUserStates[ message.chat.id ];

                    if ( currentChatUserStates ) {
                        let currentUserState = currentChatUserStates[ message.from.id ];

                        if ( currentUserState ) {
                            await this.cancelUserState( message.chat.id, currentUserState );
                        }
                    }
                }
                break;
            case "init":
                if ( await this.verifyCommandPermissionsAsync( message ) ) {
                    await this.handleInitCommandAsync( message );
                }
                break;
        }
    }

    private async verifyCommandPermissionsAsync( message: Message ) {
        if ( !message.from ) {
            return false;
        }

        let senderMember = await this.bot.getChatMember( message.chat.id, message.from.id.toString() );
        let hasPermissions = message.from.username === this.config.telegram.ownerUsername || senderMember.status === "creator" || senderMember.status === "administrator";

        if ( !hasPermissions ) {
            let noPermissionsMessage = await this.bot.sendMessage(
                message.chat.id,
                `Sorry, @${message.from.username}, but you aren't allowed to do that.`,
                { reply_to_message_id: message.message_id }
            );

            this.deleteMessagesWithDelay( DeleteMessagesDelay, [ message, noPermissionsMessage ] );
        }

        return hasPermissions;
    }

    private async handleConfigureCommandAsync( message: Message ) {
        if ( !message.from ) {
            return;
        }

        if ( message.chat.type === "channel" ) {
            return;
        } else if ( message.chat.type === "private" ) {
            await this.bot.sendMessage( message.chat.id, "That command can only be used in a Group or a Supergroup.", { reply_to_message_id: message.message_id } );

            return;
        }

        const { moderation } = this.config;
        let chatDto = await this.chatRepo.findOrCreate( message.chat.id );
        let messageText =
            `What setting would you like to change, @${message.from.username}?\n\n` +
            `*Current settings:*\n` +
            `• History Size: *${chatDto.bucketSize ?? moderation.bucketSize}*\n` +
            `• History Expire Time: *${formatSeconds( chatDto.messageDropoffTime ?? moderation.messageDropOffTime )}*\n` +
            `• Moderation Threshold: *${chatDto.moderationThreshold ?? moderation.moderationThreshold}%*\n` +
            `• Warning Timeout: *${formatSeconds( chatDto.warningCooldown ?? moderation.warningCooldown )}*`;
        let keyboard: InlineKeyboardButton[][] = [
            [
                {
                    text: "Moderation Threshold",
                    callback_data: "moderation_threshold",
                },
            ],
            [
                {
                    text: "History Size",
                    callback_data: "history_size",
                },
                {
                    text: "History Expire Time",
                    callback_data: "history_expiration",
                },
            ],
            [
                {
                    text: "Warning Timeout",
                    callback_data: "warning_timeout",
                },
                {
                    text: "Per-User Sticker Timeout",
                    callback_data: "user_sticker_cooldown",
                }
            ],
            [
                {
                    text: "Cancel",
                    callback_data: "cancel",
                },
            ],
        ];

        // Record this action as the user's last action in this chat
        let currentChatUserStates = this.chatUserStates[ message.chat.id ];

        if ( !currentChatUserStates ) {
            currentChatUserStates = this.chatUserStates[ message.chat.id ] = {};
        }

        // Delete the user's command message, if possible
        if ( await this.verifyCanDeleteAsync( message ) ) {
            await this.bot.deleteMessage( message.chat.id, message.message_id.toString() );
        }

        // Send the command initiating message
        let initiatingMessage = await this.bot.sendMessage( message.chat.id, messageText, {
            parse_mode: "MarkdownV2",
            disable_notification: true,
            reply_markup: { inline_keyboard: keyboard },
        } );

        currentChatUserStates[ message.from.id ] = {
            command: "configure",
            userId: message.from.id,
            initiatingMessageId: initiatingMessage.message_id,
        };
    }

    private async handleInitCommandAsync( message: Message ) {
        await this.chatRepo.findOrCreate( message.chat.id );

        if ( await this.verifyCanDeleteAsync( message ) ) {
            await this.bot.deleteMessage( message.chat.id, message.message_id.toString() );
        }
    }

    private async handleReplyToMeAsync( message: Message ) {
        let currentChatUserStates = this.chatUserStates[ message.chat.id ];

        if ( !currentChatUserStates || !message.from ) {
            return;
        }

        let currentUserState = currentChatUserStates[ message.from.id ];

        if ( !currentUserState ) {
            return;
        }

        switch ( currentUserState.command ) {
            case "configure": {
                if ( message.reply_to_message?.message_id !== currentUserState.promptMessageId ) {
                    return;
                }

                let parsedNumber = message.text ? +message.text : NaN;

                if ( isNaN( parsedNumber ) ) {
                    await this.cancelUserState( message.chat.id, currentUserState );

                    let tryAgainMessage = await this.bot.sendMessage(
                        message.chat.id,
                        "Sorry, I couldn't understand you...please try again and respond with a number.",
                        { reply_to_message_id: message.message_id }
                    );

                    this.deleteMessagesWithDelay( DeleteMessagesDelay, [ message, tryAgainMessage ] );
                    return;
                }

                let chatDto = await this.chatRepo.findOrCreate( message.chat.id );

                switch ( currentUserState.action ) {
                    case "history_size": {
                        chatDto.bucketSize = parsedNumber;
                        break;
                    }
                    case "history_expiration": {
                        chatDto.messageDropoffTime = parsedNumber;
                        break;
                    }
                    case "moderation_threshold": {
                        chatDto.moderationThreshold = parsedNumber;
                        break;
                    }
                    case "warning_timeout": {
                        chatDto.warningCooldown = parsedNumber;
                        break;
                    }
                    case "user_sticker_cooldown": {
                        chatDto.perUserFreeStickerCooldown = parsedNumber;
                        break;
                    }
                }

                await this.chatRepo.save( chatDto );
                await this.cancelUserState( message.chat.id, currentUserState );

                let allDoneMessage = await this.bot.sendMessage(
                    message.chat.id,
                    "Okay, all set! I've finished making your change.",
                    { reply_to_message_id: message.message_id },
                );

                this.deleteMessagesWithDelay( DeleteMessagesDelay, [ message, allDoneMessage ] );

                break;
            }
        }
    }

    private async onCallbackQuery( query: CallbackQuery ) {
        try {
            // Answer the query immediately
            await this.bot.answerCallbackQuery( query.id );

            if ( !query.message ) {
                return;
            }

            let currentChatUserStates = this.chatUserStates[ query.message.chat.id ];

            if ( !currentChatUserStates ) {
                return;
            }

            let currentUserState = currentChatUserStates[ query.from.id ];

            if ( !currentUserState || currentUserState.initiatingMessageId !== query.message?.message_id ) {
                // Silently fail if we get an unknown callback query

                return;
            }

            switch ( currentUserState.command ) {
                case "configure": {
                    await this.handleConfigureQueryAsync( query, currentUserState );
                    break;
                }
                default: {
                    // Unknown query command; delete our knowledge of this user

                    delete currentChatUserStates[ query.from.id ];
                    break;
                }
            }
        } catch ( err ) {
            this.onError( err );
        }
    }

    private async handleConfigureQueryAsync( query: CallbackQuery, state: UserCommandState ) {
        let promptText: string;

        switch ( query.data ) {
            case "history_size":
                promptText = "How many messages should I keep track of to analyze sticker frequency?"
                break;
            case "history_expiration":
                promptText = "After how many seconds should I forget that a message was sent?";
                break;
            case "moderation_threshold":
                promptText =
                    "At what sticker-to-chat ratio should I start deleting stickers? " +
                    "Give the response as a percentage.\n\nFor example, reply with \"25\" to " +
                    "have me start deleting stickers if 25% of the messages in my history were a sticker.";
                break;
            case "warning_timeout":
                promptText =
                    "How often (in seconds) should I send a warning message to a user for using " +
                    "too many stickers (instead of simply deleting the sticker)?";
                break;
            case "user_sticker_cooldown":
                promptText =
                    "How often (in seconds) should any given user be able to use a sticker " +
                    "regardless of the chat threshold?";
                break;
            default:
                // Simply try and delete the prompt message since the user canceled
                await this.cancelUserState( query.message!.chat.id, state );
                return;
        }

        let promptMessage = await this.bot.sendMessage(
            query.message!.chat.id,
            `@${query.from.username}: ${promptText}`,
            {
                reply_markup: {
                    force_reply: true,
                    selective: true,
                },
            }
        );

        state.action = query.data;
        state.promptMessageId = promptMessage.message_id;
    }

    private async cancelUserState( chatId: number, userState: UserCommandState ) {
        if ( userState.initiatingMessageId ) {
            await this.bot.deleteMessage( chatId, userState.initiatingMessageId.toString() );
        }

        if ( userState.promptMessageId ) {
            await this.bot.deleteMessage( chatId, userState.promptMessageId.toString() );
        }

        let currentChatUserStates = this.chatUserStates[ chatId ];

        if ( currentChatUserStates ) {
            delete currentChatUserStates[ userState.userId ];
        }
    }

    private deleteMessagesWithDelay( delay: number, messages: Message[] ) {
        setTimeout( async () => {
            for ( let message of messages ) {
                try {
                    if ( await this.verifyCanDeleteAsync( message ) ) {
                        await this.bot.deleteMessage( message.chat.id, message.message_id.toString() );
                    }
                } catch ( err ) {
                    this.onError( err );
                }
            }
        }, delay );
    }

    private onError( error: Error ) {
        console.error( "=== Telegram Error ===" );
        console.error( error );
    }
}