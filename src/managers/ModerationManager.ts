import { singleton } from "tsyringe";
import { Message, MessageType } from "node-telegram-bot-api";
import { Repository, Connection } from "typeorm";
import { MessageDto } from "@/models/data/MessageDto";
import { ModerationConfig } from "@/models/config";
import { ConfigManager } from "@/managers/ConfigManager";
import { ChatRepository } from "@/repos/ChatRepository";
import { UserRepository } from "@/repos/UserRepository";

import moment from "moment";

@singleton()
export class ModerationManager {
    private config: ModerationConfig;
    private chatRepo: ChatRepository;
    private userRepo: UserRepository;
    private messageRepo: Repository<MessageDto>;

    constructor( configManager: ConfigManager, dataConnection: Connection ) {
        this.config = configManager.config.moderation;
        this.chatRepo = dataConnection.getCustomRepository( ChatRepository );
        this.userRepo = dataConnection.getCustomRepository( UserRepository );
        this.messageRepo = dataConnection.getRepository( MessageDto );
    }

    public async recordEntryAsync( type: MessageType, message: Message ) {
        if ( !message.from ) {
            console.error( `Error: Message "${message.message_id}" did not have a "from" property` );
            return;
        }

        // Ensure a row exists for the chat so that FK constraint never fails
        await this.chatRepo.findOrCreate( message.chat.id );

        let messageDto = this.messageRepo.create( {
            id: message.message_id,
            chatId: message.chat.id,
            date: moment.unix( message.date ).utc().toDate(),
            fromUserId: message.from.id,
            type,
        } );

        await this.messageRepo.save( messageDto );

        if ( type === "sticker" ) {
            let chatUserDto = await this.userRepo.findOrCreate( message.chat.id, message.from.id );

            chatUserDto.lastSticker = moment.utc().toDate();

            await this.userRepo.save( chatUserDto );
        }
    }

    public async shouldModerateAsync( type: MessageType, message: Message ) {
        if ( type !== "sticker" || !message.from ) {
            // If it's not a sticker or somehow we don't know the sender, don't moderate it

            return false;
        }

        if ( !await this.chatRepo.isOverThresholdAsync( message.chat.id ) ) {
            // Don't moderate anything if we're not currently over the noise threshold

            return false;
        }

        let chatUserDto = await this.userRepo.findOrCreate( message.chat.id, message.from.id );

        if ( !chatUserDto.lastSticker ) {
            // If the user has not used a sticker in known history, don't moderate it

            return false;
        }

        let chatDto = await this.chatRepo.findOneOrFail( message.chat.id );
        let userLastSticker = moment.utc( chatUserDto.lastSticker );
        let timeSinceLastSticker = moment.utc().diff( userLastSticker, "seconds" );
        let perUserFreeStickerCooldown = chatDto.perUserFreeStickerCooldown ?? this.config.perUserFreeStickerCooldown;

        // Moderate if the user's last sticker was within the drop-off time (i.e. allow users
        // to send exactly one sticker even if the thrshold is reached)
        return timeSinceLastSticker < perUserFreeStickerCooldown;
    }

    public async shouldWarnSenderAsync( message: Message ) {
        if ( !message.from ) {
            return false;
        }

        let chatUserDto = await this.userRepo.findOrCreate( message.chat.id, message.from.id );

        if ( !chatUserDto.lastWarning ) {
            return true;
        }

        let chatDto = await this.chatRepo.findOneOrFail( message.chat.id );
        let userLastWarned = moment.utc( chatUserDto.lastWarning );
        let timeSinceWarned = moment.utc().diff( userLastWarned, "seconds" );
        let warningCooldown = chatDto.warningCooldown ?? this.config.warningCooldown;

        return timeSinceWarned >= warningCooldown;
    }

    public async notifyWarnedSenderAsync( message: Message ) {
        if ( !message.from ) {
            return;
        }

        let chatUserDto = await this.userRepo.findOrCreate( message.chat.id, message.from.id );

        chatUserDto.lastWarning = moment.utc().toDate();

        await this.userRepo.save( chatUserDto );
    }
}