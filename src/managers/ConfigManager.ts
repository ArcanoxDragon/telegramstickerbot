import { singleton } from "tsyringe";
import { readFile } from "@/utility/fs-promises";
import { fileExists } from "@/utility/fs-utility";
import { parse as parseJsonc } from "jsonc-parser";
import { ConfigFile } from "@/models/config";

import _ from "lodash";
import path from "path";

const DefaultConfigPath = path.join( __dirname, "../../config.default.json" );

@singleton()
export class ConfigManager {
    private _config: Nullable<ConfigFile> = null;

    get config() {
        if ( this._config == null ) {
            throw new Error( "Config must be loaded before it can be accessed" );
        }

        return this._config;
    }

    public async loadAsync( path: string ) {
        if ( !await fileExists( DefaultConfigPath ) ) {
            throw new Error( `Default config file does not exist: ${DefaultConfigPath}` );
        }

        if ( !await fileExists( path ) ) {
            throw new Error( `Specified config file does not exist: ${path}` );
        }

        let defaultConfigJson = await readFile( DefaultConfigPath, { encoding: "utf8" } );
        let configJson = await readFile( path, { encoding: "utf8" } );
        let config: Partial<ConfigFile> = {};

        _.merge( config, parseJsonc( defaultConfigJson ), parseJsonc( configJson ) );
        this._config = config as ConfigFile;
    }
}