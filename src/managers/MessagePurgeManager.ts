import { singleton } from "tsyringe";
import { Repository, Connection, LessThan } from "typeorm";
import { MessageDto } from "@/models/data/MessageDto";
import { ChatRepository } from "@/repos/ChatRepository";

import moment from "moment";
import { ConfigManager } from "@/managers/ConfigManager";
import { ConfigFile } from "@/models/config";

const Interval = moment.duration( 5, "minutes" ).asMilliseconds();

@singleton()
export class MessagePurgeManager {
    private messageRepo: Repository<MessageDto>;
    private chatRepo: ChatRepository;
    private config: ConfigFile;
    private active: boolean;
    private timeoutId?: NodeJS.Timeout;

    constructor( dataConnection: Connection, configManager: ConfigManager ) {
        this.messageRepo = dataConnection.getRepository( MessageDto );
        this.chatRepo = dataConnection.getCustomRepository( ChatRepository );
        this.config = configManager.config;
        this.active = false;
    }

    public start() {
        this.active = true;
        this.timeoutId = setTimeout( this.purgeOldMessagesAsync.bind( this ), Interval );
    }

    public stop() {
        this.active = false;

        if ( this.timeoutId ) {
            clearTimeout( this.timeoutId );
            this.timeoutId = undefined;
        }
    }

    private async purgeOldMessagesAsync() {
        try {
            let allKnownChats = await this.chatRepo.find();

            for ( let chat of allKnownChats ) {
                let messageDropoffTime = chat.messageDropoffTime ?? this.config.moderation.messageDropOffTime;
                let deleteBeforeTime = moment.utc().subtract( messageDropoffTime, "seconds" );

                // Delete all the old messages
                await this.messageRepo.delete( {
                    chatId: chat.id,
                    date: LessThan( deleteBeforeTime.toDate() ),
                } );
            }
        } catch ( err ) {
            console.error( "=== Error Purging Old Messages ===" );
            console.error( err );
        } finally {
            if ( this.active ) {
                this.timeoutId = setTimeout( this.purgeOldMessagesAsync.bind( this ), Interval );
            }
        }
    }
}