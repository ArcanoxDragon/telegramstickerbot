import moment from "moment";

const DurationFormat = "d[d] h[h] m[m] s[s]";

export function formatSeconds( seconds: number ) {
    return moment.duration( seconds, "seconds" ).format( DurationFormat, { trim: "both" } );
}