import { stat } from "@/utility/fs-promises";

export async function fileExists( path: string ): Promise<boolean> {
    try {
        let stats = await stat( path );

        return stats && stats.isFile();
    } catch {
        return false;
    }
}

export async function directoryExists( path: string ): Promise<boolean> {
    try {
        let stats = await stat( path );

        return stats && stats.isDirectory();
    } catch {
        return false;
    }
}