import { MysqlConnectionOptions } from "typeorm/driver/mysql/MysqlConnectionOptions";
import { createConnection } from "typeorm";

import path from "path";

export async function createDataConnection( config: MysqlConnectionOptions ) {
    return await createConnection( {
        ...config,
        type: "mariadb",
        bigNumberStrings: false,
        entities: [
            path.join( __dirname, "../models/data/*.ts" )
        ],
        synchronize: false,
    } );
}