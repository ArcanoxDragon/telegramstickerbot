import { MysqlConnectionOptions } from "typeorm/driver/mysql/MysqlConnectionOptions";

export interface ConfigFile {
    database: MysqlConnectionOptions;
    moderation: ModerationConfig;
    telegram: TelegramConfig;
    webhook: WebhookConfig;
}

export interface ModerationConfig {
    bucketSize: number;
    normalNoiseScore: number;
    stickerNoiseScore: number;
    moderationThreshold: number;
    messageDropOffTime: number;
    warningCooldown: number;
    perUserFreeStickerCooldown: number;
}

export interface TelegramConfig {
    botToken: string;
    ownerUsername: string;
}

export interface WebhookConfig {
    address: string;
    port: number;
    sslCertPath: string;
    sslKeyPath: string;
}