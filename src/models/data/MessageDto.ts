import { Column, Entity, ManyToOne, JoinColumn, PrimaryColumn } from "typeorm";
import { MessageType, Message } from "node-telegram-bot-api";
import { ChatDto } from "@/models/data/ChatDto";

@Entity( "Message" )
export class MessageDto {
    @PrimaryColumn( { type: "bigint" } )
    id!: number;

    @Column( { type: "varchar" } )
    type!: MessageType;

    @Column( { type: "datetime" } )
    date!: Date;

    @Column( { type: "bigint" } )
    chatId!: number;

    @Column( { type: "bigint" } )
    fromUserId!: number;

    @ManyToOne( type => ChatDto, chat => chat.messages )
    @JoinColumn()
    chat!: Promise<ChatDto>;
}
