import { Column, PrimaryColumn, Entity, OneToMany, JoinTable } from "typeorm";
import { MessageDto } from "@/models/data/MessageDto";
import { Message } from "node-telegram-bot-api";

@Entity( "Chat" )
export class ChatDto {
    @PrimaryColumn( { type: "bigint" } )
    id!: number;

    @Column( { type: "int" } )
    bucketSize?: number;

    @Column( { type: "int" } )
    normalNoiseScore?: number;

    @Column( { type: "int" } )
    stickerNoiseScore?: number;

    @Column( { type: "int" } )
    moderationThreshold?: number;

    @Column( { type: "int" } )
    messageDropoffTime?: number;

    @Column( { type: "int" } )
    warningCooldown?: number;

    @Column( { type: "int" } )
    perUserFreeStickerCooldown?: number;

    @OneToMany( type => MessageDto, message => message.chat )
    @JoinTable()
    messages!: Promise<MessageDto[]>;
}
