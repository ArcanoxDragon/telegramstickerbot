import { Column, PrimaryColumn, Entity } from "typeorm";

@Entity( "ChatUser" )
export class ChatUserDto {
    @PrimaryColumn( { type: "bigint" } )
    userId!: number;

    @PrimaryColumn( { type: "bigint" } )
    chatId!: number;

    @Column( { type: "timestamp" } )
    lastWarning?: Date;

    @Column( { type: "timestamp" } )
    lastSticker?: Date;
}
