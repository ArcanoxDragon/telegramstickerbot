FROM node:14.5-alpine
WORKDIR /usr/app

# Install extra packages
RUN apk add --no-cache curl tzdata

# Copy dependency manifest and install dependencies
COPY [ "package.json", "yarn.lock", "./" ]
RUN yarn install

# Copy files less likely to change
COPY [ "tsconfig.json", "config.default.json", "config.schema.json", "./" ]

# Copy source code itself
COPY "src/" "src/"

# Set up volumes
VOLUME "config/" "ssl/"

# Set up args
ENV CONFIG_PATH="config/config.json"

# Set up entrypoint command
CMD [ "sh", "-c", "yarn start -c $CONFIG_PATH" ]