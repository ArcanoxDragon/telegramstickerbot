create or replace table Chat
(
	id bigint not null,
	bucketSize int default 50 not null,
	normalNoiseScore decimal default 0 not null,
	stickerNoiseScore decimal default 100 not null,
	moderationThreshold decimal default 65 not null,
	messageDropoffTime int default 7200 not null,
	warningCooldown int default 600 not null,
	constraint chats_pk
		primary key (id)
);

create or replace table ChatUser
(
    userId bigint not null,
    chatId bigint not null,
	lastWarning timestamp null,
	lastSticker timestamp null,
	constraint chat_users_pk
        primary key (userId, chatId),
	constraint chat_users_chat_id_fk
        foreign key (chatId) references Chat (id)
            on delete cascade
);

create or replace table Message
(
	id bigint not null,
	type varchar(30) not null,
	date datetime not null,
	chatId bigint not null,
	fromUserId bigint not null,
	constraint message_pk
		primary key (id),
    constraint message_chat_id_fk
		foreign key (chatId) references Chat (id)
			on delete cascade
);